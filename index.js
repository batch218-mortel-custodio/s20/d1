///////////////////////////////////// While Loop 
/*
		- expressions are any unit of code that can be evaluated to a value
		-  if the condition evaluates to true, the statements inside the block will be executed.
		(The while loop loops through a block of code as long as a specified condition is true)
		
*/

/*let count = 10;

while(count !== 1){ // process 1: checks if the condition here is true first. it will only stop the loop if the condition will be false.
	console.log("Count: "+ count); // process 2: console print
	count--; // process 3: decrements count value from 10 to 9, then 9 to 8, and so on.
}*/

//////////////////////////// Do While Loop

/*let number = Number(prompt("Give mah a numba womba: ")); // Number function capital N  = parseInt function??

// after executing once, the while statement will evaluatte whether to run the next iteration of the loop based on given expression/condition (e.g. number less than 10)
// If the conditon is not true, the loop will stop
do{
	console.log("Number: "+number); //process 2(TWO)!!
	number += 1; // or number++l << iz the zame
} while(number < 10);//process 1(ONE)!! = DEBUNKED BY TEACHER(DO 1st then while)*/



// let na = 15;
// while(na < 10){
// 	console.log("Numbahr: "+ na);
// 	na++;
// }


//////////////////////////////// FOR LOOP
	//initialization //conditi  //change of value/instruction
for(let count = 10; count <= 20; count++){
	console.log(count)
};

let myString = "tupe";
console.log("myString length: " + myString.length);

console.log(myString[1]);

for (i=0; i < myString.length; i++){
	console.log(myString[i]);
}

/*let i = 0
while( i < myString.length){
	console.log(myString[i]);
	i++;
}*/


let myName  = "ALEx";
for (let i=0; i<myName.length; i++){
	if(
			myName[i].toLowerCase() == "a" ||
			myName[i].toLowerCase() == "e" ||
			myName[i].toLowerCase() == "i" ||
			myName[i].toLowerCase() == "o" ||
			myName[i].toLowerCase() == "u"
		){
			console.log(3);
		}
	else{
			console.log(myName[i]);
		}
}